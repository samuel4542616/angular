// Importa el decorador Injectable desde el módulo core de Angular
import { Injectable } from '@angular/core';

// Importa la clase HttpClient desde el módulo http de Angular
import { HttpClient } from '@angular/common/http';

// Importa la clase Observable desde la biblioteca rxjs
import { Observable } from 'rxjs';

// Importa el modelo de datos Post desde el archivo post.model.ts ubicado en la carpeta models
import { Post } from '../models/post.model'; 

// Decorador Injectable: indica que la clase puede ser inyectada con dependencias y especifica que este servicio se proporcionará en el nivel raíz
@Injectable({
  providedIn: 'root'
})
// Declara la clase PublicacionesService
export class PublicacionesService {
  // Constructor del servicio que recibe una instancia de HttpClient como parámetro
  constructor(private http: HttpClient) { }

  // Método para obtener las publicaciones desde la API
  // Devuelve un Observable que emite un array de objetos Post
  getPublicaciones(): Observable<Post[]> {
    // Realiza una solicitud GET a la URL especificada y espera una respuesta que se espera que sea un array de objetos Post
    return this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts');
  }
}
