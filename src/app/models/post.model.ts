// Define una interfaz llamada 'Post'
export interface Post {
    // Propiedad 'id' de tipo number que representa el identificador del post
    id: number;
    // Propiedad 'title' de tipo string que representa el título del post
    title: string;
    // Propiedad 'body' de tipo string que representa el contenido del post
    body: string;
  }
  