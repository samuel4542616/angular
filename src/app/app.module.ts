// Le decimos a la computadora que vamos a usar Angular.
import { NgModule } from '@angular/core';

// Esto es como una caja de herramientas especial para hacer páginas web.
import { BrowserModule } from '@angular/platform-browser';

// Esto nos ayuda a obtener cosas de Internet.
import { HttpClientModule } from '@angular/common/http';

// Este es como un mapa que nos dice cómo navegar por nuestra página web.
import { AppRoutingModule } from './app-routing.module';

// Aquí traemos las partes que hicimos para nuestra página web, como el aspecto y las diferentes secciones.
import { AppComponent } from './app.component';
import { InicioComponent } from './paginas/inicio/inicio.component';
import { NosotrosComponent } from './paginas/nosotros/nosotros.component';
import { ContactenosComponent } from './paginas/contactenos/contactenos.component';
import { PublicacionesComponent } from './paginas/publicaciones/publicaciones.component';

// Esto es como una lista de las cosas que necesitamos para que nuestra página web funcione correctamente.
@NgModule({
  // Aquí ponemos las partes que hicimos y que queremos usar en nuestra página.
  declarations: [
    AppComponent,
    InicioComponent,
    NosotrosComponent,
    ContactenosComponent,
    PublicacionesComponent
  ],
  // Aquí ponemos las herramientas especiales que necesitamos para hacer nuestra página.
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  // Esto no lo necesitamos ahora, pero es como traer a las personas que nos ayudan a hacer nuestra página web.
  providers: [],
  // Aquí decimos cuál es la parte principal de nuestra página web.
  bootstrap: [AppComponent]
})
// Esto es como decirle a la computadora que todo lo que estamos haciendo aquí es parte de nuestra página web.
export class AppModule { }
