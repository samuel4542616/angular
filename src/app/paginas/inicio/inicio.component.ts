// Importa la clase 'Component' del módulo '@angular/core'
import { Component } from '@angular/core';

// Decorador @Component para definir metadatos del componente
@Component({
  // Selector CSS que identifica al componente en el HTML
  selector: 'app-inicio',
  // URL del archivo HTML que contiene la plantilla del componente
  templateUrl: './inicio.component.html',
  // URL del archivo CSS que contiene los estilos del componente
  styleUrls: ['./inicio.component.css']
})
// Clase del componente InicioComponent
export class InicioComponent {
  // Aquí podrías definir propiedades y métodos del componente
}
