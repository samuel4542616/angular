// Importa las funciones ComponentFixture y TestBed desde el módulo '@angular/core/testing'
import { ComponentFixture, TestBed } from '@angular/core/testing';

// Importa el componente InicioComponent desde el archivo './inicio.component'
import { InicioComponent } from './inicio.component';

// Describe un conjunto de pruebas para el componente InicioComponent
describe('InicioComponent', () => {
  // Declara las variables component y fixture para el componente InicioComponent
  let component: InicioComponent;
  let fixture: ComponentFixture<InicioComponent>;

  // Define una función beforeEach para ejecutar acciones antes de cada prueba
  beforeEach(async () => {
    // Configura el módulo de prueba con TestBed
    await TestBed.configureTestingModule({
      declarations: [InicioComponent] // Declara el componente InicioComponent
    })
    .compileComponents(); // Compila los componentes declarados en el TestBed
    
    // Crea una instancia del componente InicioComponent y un ComponentFixture asociado a él
    fixture = TestBed.createComponent(InicioComponent);
    // Obtiene la instancia del componente InicioComponent
    component = fixture.componentInstance;
    // Detecta los cambios en el componente y en sus hijos
    fixture.detectChanges();
  });

  // Define una prueba que verifica si el componente InicioComponent se crea correctamente
  it('should create', () => {
    // Se espera que el componente no sea nulo (que exista)
    expect(component).toBeTruthy();
  });
});
