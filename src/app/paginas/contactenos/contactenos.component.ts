// Importamos la funcionalidad 'Component' desde la librería '@angular/core'
import { Component } from '@angular/core';

// Definimos un nuevo componente llamado 'ContactenosComponent'
@Component({
  // Selector que se utilizará para incrustar este componente en el HTML
  selector: 'app-contactenos',
  // Ruta al archivo HTML que contiene la plantilla del componente
  templateUrl: './contactenos.component.html',
  // Ruta al archivo CSS que contiene los estilos del componente
  styleUrl: './contactenos.component.css'
})

// Clase del componente 'ContactenosComponent'
export class ContactenosComponent {
  // Aquí podemos agregar lógica específica para este componente si es necesario
}
