// Importamos la funcionalidad 'Component' desde la librería '@angular/core'
import { Component } from '@angular/core';

// Definimos un nuevo componente llamado 'NosotrosComponent'
@Component({
  // Selector que se utilizará para incrustar este componente en el HTML
  selector: 'app-nosotros',
  // Ruta al archivo HTML que contiene la plantilla del componente
  templateUrl: './nosotros.component.html',
  // Ruta al archivo CSS que contiene los estilos del componente
  styleUrl: './nosotros.component.css'
})

// Clase del componente 'NosotrosComponent'
export class NosotrosComponent {
  // Aquí podemos agregar lógica específica para este componente si es necesario
}
