// Importamos Component y OnInit desde '@angular/core'
import { Component, OnInit } from '@angular/core';
// Importamos el modelo Post desde '../../models/post.model'
import { Post } from '../../models/post.model';
// Importamos el servicio PublicacionesService desde '../../servicios/publicaciones.service'
import { PublicacionesService } from '../../servicios/publicaciones.service';

// Definimos el componente 'PublicacionesComponent' e implementamos OnInit
@Component({
  selector: 'app-publicaciones', // Selector del componente
  templateUrl: './publicaciones.component.html', // Plantilla HTML del componente
  styleUrls: ['./publicaciones.component.css'] // Archivo de estilos del componente
})
export class PublicacionesComponent implements OnInit {
  posts: Post[] = []; // Arreglo de publicaciones inicializado como vacío

  // Constructor del componente, inyectamos el servicio PublicacionesService
  constructor(private publicacionesService: PublicacionesService) { }

  // Método ngOnInit se ejecuta después de que Angular muestra las propiedades vinculadas en una vista
  ngOnInit(): void {
    // Llamamos al método getPublicaciones al inicializar el componente
    this.getPublicaciones();
  }

  // Método para obtener las publicaciones desde el servicio
  getPublicaciones(): void {
    // Llamamos al método getPublicaciones del servicio, nos suscribimos para obtener los datos
    this.publicacionesService.getPublicaciones()
      .subscribe(posts => this.posts = posts); // Asignamos las publicaciones obtenidas al arreglo posts
  }
}
