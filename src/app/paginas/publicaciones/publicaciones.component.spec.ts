// Importamos funcionalidades de prueba desde '@angular/core/testing'
import { ComponentFixture, TestBed } from '@angular/core/testing';

// Importamos el componente 'PublicacionesComponent' que vamos a probar
import { PublicacionesComponent } from './publicaciones.component';

// Describimos una serie de pruebas para el componente 'PublicacionesComponent'
describe('PublicacionesComponent', () => {
  // Declaramos variables para almacenar el componente y su fixture (entorno de pruebas)
  let component: PublicacionesComponent;
  let fixture: ComponentFixture<PublicacionesComponent>;

  // Configuramos las pruebas antes de ejecutarlas
  beforeEach(async () => {
    // Configuramos un módulo de pruebas con el componente 'PublicacionesComponent'
    await TestBed.configureTestingModule({
      declarations: [PublicacionesComponent]
    }).compileComponents(); // Compilamos los componentes del módulo

    // Creamos una instancia del componente y su fixture
    fixture = TestBed.createComponent(PublicacionesComponent);
    component = fixture.componentInstance;

    // Detectamos los cambios en el componente
    fixture.detectChanges();
  });

  // Especificamos una prueba para verificar que el componente se crea correctamente
  it('should create', () => {
    expect(component).toBeTruthy(); // Esperamos que el componente exista
  });
});
